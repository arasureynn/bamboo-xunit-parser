[#-- @ftlvariable name="uiConfigSupport" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[#assign addExecutableLink][@ui.displayAddExecutableInline executableKey='xunit' /][/#assign]
[@ww.select cssClass="builderSelectWidget" labelKey='executable.type' name='label'
list=uiConfigSupport.getExecutableLabels('xunit')
extraUtility=addExecutableLink required='true' /]

[@ww.select cssClass="builderSelectWidget" labelKey='xunit.runner.parallelValues' name='label'
list="com.reynn.bamboo.task.XunitRunnerTask.parallelValues"
/]

[@ww.textfield labelKey='xunit.testFiles' name='xunitTestFiles' required='true' cssClass="long-field" /]
[@ww.textfield labelKey='xunit.resultsFile' name='xunitResultsFile' required='true' cssClass="long-field" /]
[@ww.textfield labelKey='xunit.testsToRun' name='run' cssClass="long-field" /]
[@ww.textfield labelKey='xunit.options' name='commandLineOptions' cssClass="long-field" /]
[@ww.textfield labelKey='builder.common.env' name='environmentVariables' cssClass="long-field" /]