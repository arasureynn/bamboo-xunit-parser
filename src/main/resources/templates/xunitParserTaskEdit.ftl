[@ww.textfield labelKey='builder.common.tests.directory.custom' name='testResultsDirectory' cssClass="long-field" required="true" /]

[@ui.bambooSection titleKey='builder.common.tests.enabled' collapsible=true]
    [@ww.checkbox labelKey="builder.common.tests.pickup.outdated.files" name="pickupOutdatedFiles"/]
[/@ui.bambooSection]
