package com.reynn.bamboo.task;

import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import com.atlassian.utils.process.ExternalProcess;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.logging.Logger;

public class XunitRunnerTask implements TaskType {

    private final ProcessService processService;

    public static final String NUNIT_CAPABILITY_PREFIX = CapabilityDefaultsHelper.CAPABILITY_BUILDER_PREFIX + ".nunit";

    public static final String LABEL = "label";
    public static final String TEST_FILES = "xunitTestFiles";
    public static final String RESULTS_FILE = "xunitResultsFile";
    public static final String TESTS_TO_RUN = "run"; // eg. /run:NUnit.Tests.AssertionTests,NUnit.Tests.ConstraintTests
    public static final String INCLUDED_TEST_CATEGORIES = "include"; // eg. /include:TheseTests,AndTheseTests
    public static final String EXCLUDED_TEST_CATEGORIES = "exclude"; // eg. /exclude:BrokenTests
    public static final String COMMAND_OPTIONS = "commandLineOptions";
    public static final String ENVIRONMENT = "environmentVariables";

    public static final String[] parallelValues = new String[]{"All", "Collections", "Assemblies"};
    public static final String[] maxThreadsValues = new String[]{"0","1","2","3","4","5","6","7","8"};

    public XunitRunnerTask(final ProcessService processService) {
        this.processService = processService;
    }

    @NotNull
    public TaskResult execute(@NotNull final TaskContext taskContext) throws TaskException {
        TaskResultBuilder builder = TaskResultBuilder.newBuilder(taskContext);

        ExternalProcess process = processService.createExternalProcess(taskContext,
                new ExternalProcessBuilder()
                        .command(Arrays.asList("/bin/ls"))
                        .workingDirectory(taskContext.getWorkingDirectory())
        );

        process.execute();

        return builder.checkReturnCode(process, 0).build();
    }
}