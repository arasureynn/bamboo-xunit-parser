package com.reynn.bamboo.task;

import com.atlassian.bamboo.build.logger.interceptors.ErrorMemorisingInterceptor;
import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.task.*;
import com.atlassian.bamboo.v2.build.CurrentResult;
import com.reynn.bamboo.parser.XunitParser;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class XunitParserTask implements TaskType {

    private final TestCollationService testCollationService;
    private static final Logger log = Logger.getLogger(XunitParserTask.class);

    public XunitParserTask(TestCollationService testCollationService) {
        this.testCollationService = testCollationService;
    }

    @NotNull
    @Override
    public TaskResult execute(@NotNull TaskContext taskContext) throws TaskException {
        CurrentResult currentResult = taskContext.getCommonContext().getCurrentResult();
        ErrorMemorisingInterceptor errorLines = ErrorMemorisingInterceptor.newInterceptor();
        taskContext.getBuildLogger().getInterceptorStack().add(errorLines);

        try {
            this.testCollationService.collateTestResults(taskContext,
                    (String)taskContext.getConfigurationMap().get("testResultsDirectory"),
                    new XunitTestReportCollector(),
                    taskContext.getConfigurationMap().getAsBoolean("pickupOutdatedFiles"));


            return TaskResultBuilder.newBuilder(taskContext).checkTestFailures().build();
        } catch (Exception e) {
            throw new TaskException("Failed to execute task: " + e.getLocalizedMessage());
        } finally {
            currentResult.addBuildErrors(errorLines.getErrorStringList());
        }
    }
}