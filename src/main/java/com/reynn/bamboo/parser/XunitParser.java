package com.reynn.bamboo.parser;

import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.resultsummary.tests.TestCaseResultErrorImpl;
import com.atlassian.bamboo.resultsummary.tests.TestState;
import com.google.common.collect.Lists;
import net.jodah.xsylum.XmlDocument;
import net.jodah.xsylum.XmlElement;
import net.jodah.xsylum.Xsylum;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.util.List;

public class XunitParser {

    private static final Logger log = Logger.getLogger(XunitParser.class);

    private List<TestResults> successfulTests = Lists.newArrayList();
    private List<TestResults> failingTests = Lists.newArrayList();
    private List<TestResults> skippedTests = Lists.newArrayList();

    public void parse(@NotNull InputStream inputStream) throws Exception {

        XmlDocument doc = Xsylum.documentFor(inputStream);

        List<XmlElement> nList = doc.getAll("test");

        for (XmlElement element : nList) {
            String className = element.attribute("type");
            String methodName = element.attribute("method");
            String duration = element.attribute("time");
            String status = element.attribute("result");

            TestResults results = new TestResults(className, methodName, duration);
            switch (status.toLowerCase()) {
                case "fail":
                    results.setState(TestState.FAILED);
                    XmlElement failureReport = element.get("failure");
                    if (failureReport != null) {
                        results.addError(new TestCaseResultErrorImpl(failureReport.value()));
                    }
                    failingTests.add(results);
                    break;
                case "pass":
                    results.setState(TestState.SUCCESS);
                    successfulTests.add(results);
                    break;
                case "skip":
                    results.setState(TestState.SKIPPED);
                    XmlElement skipReport = element.get("reason");
                    results.addError(new TestCaseResultErrorImpl(skipReport.value()));
                    skippedTests.add(results);
                    break;
            }
        }
    }

    @NotNull
    public List<TestResults> getSuccessfulTests()
    {
        return this.successfulTests;
    }

    @NotNull
    public List<TestResults> getFailedTests()
    {
        return this.failingTests;
    }

    @NotNull
    public List<TestResults> getSkippedTests()
    {
        return this.skippedTests;
    }
}