package com.reynn.bamboo.configurator;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskTestResultsSupport;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BaseTestResultConfigurator
        extends AbstractTaskConfigurator
        implements TaskTestResultsSupport {
    private static final Logger log = Logger.getLogger(BaseTestResultConfigurator.class);
    private static final String DEFAULT_TEST_RESULTS_PATTERN = "**/test-reports/*.xml";
    private static final List<String> FIELDS_TO_COPY = ImmutableList.of("testResultsDirectory", "pickupOutdatedFiles");
    private I18nResolver i18nResolver;

    @NotNull
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params, @Nullable TaskDefinition previousTaskDefinition)
    {
        Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        this.taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(config, params, FIELDS_TO_COPY);

        return config;
    }

    public void populateContextForView(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition)
    {
        this.taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition)
    {
        this.taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    public void populateContextForCreate(@NotNull Map<String, Object> context)
    {
        context.put("testResultsDirectory", "**/test-reports/*.xml");
        context.put("pickupOutdatedFiles", Boolean.FALSE);
    }

    protected void validateTestResultsFilePattern(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection)
    {
        if (StringUtils.isEmpty(params.getString("testResultsDirectory"))) {
            errorCollection.addError("testResultsDirectory", this.i18nResolver.getText("task.generic.validate.testResultsPattern.mandatory"));
        }
    }

    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection)
    {
        validateTestResultsFilePattern(params, errorCollection);
    }

    public boolean taskProducesTestResults(@NotNull TaskDefinition taskDefinition)
    {
        return true;
    }

    public void setI18nResolver(I18nResolver i18nResolver)
    {
        this.i18nResolver = i18nResolver;
    }
}
