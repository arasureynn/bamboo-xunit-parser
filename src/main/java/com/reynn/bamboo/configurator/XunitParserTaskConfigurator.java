package com.reynn.bamboo.configurator;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * Created by nic on 5/13/2014.
 */
public class XunitParserTaskConfigurator extends BaseTestResultConfigurator {

    private static final String DEFAULT_TEST_RESULTS_PATTERN = "Results/*";

    public void populateContextForCreate(@NotNull Map<String, Object> context)
    {
        context.put("testResultsDirectory", DEFAULT_TEST_RESULTS_PATTERN);
        context.put("pickupOutdatedFiles", Boolean.FALSE);
    }
}
