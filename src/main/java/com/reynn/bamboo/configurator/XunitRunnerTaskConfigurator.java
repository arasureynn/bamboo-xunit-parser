package com.reynn.bamboo.configurator;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.BuildTaskRequirementSupport;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskTestResultsSupport;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.agent.capability.Requirement;
import com.google.common.collect.Sets;
import com.reynn.bamboo.task.XunitRunnerTask;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.Set;

public class XunitRunnerTaskConfigurator extends BaseRunnerTaskConfigurator implements TaskTestResultsSupport, BuildTaskRequirementSupport{

    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(XunitRunnerTaskConfigurator.class);

    private static final String DEFAULT_TEST_FILE = "TestResult.xml";

    private static final Set<String> FIELDS_TO_COPY = Sets.newHashSet(
            XunitRunnerTask.LABEL,
            XunitRunnerTask.TEST_FILES,
            XunitRunnerTask.RESULTS_FILE,
            XunitRunnerTask.TESTS_TO_RUN,
            XunitRunnerTask.INCLUDED_TEST_CATEGORIES,
            XunitRunnerTask.EXCLUDED_TEST_CATEGORIES,
            XunitRunnerTask.COMMAND_OPTIONS,
            XunitRunnerTask.ENVIRONMENT
    );

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);

        final String label = params.getString(XunitRunnerTask.LABEL);
        if (StringUtils.isEmpty(label))
        {
            errorCollection.addError(XunitRunnerTask.LABEL, i18nResolver.getText("xunit.label.error"));
        }

        final String testFiles = params.getString(XunitRunnerTask.TEST_FILES);
        if (StringUtils.isEmpty(testFiles))
        {
            errorCollection.addError(XunitRunnerTask.TEST_FILES, i18nResolver.getText("xunit.testFiles.error"));
        }

        final String resultsFile = params.getString(XunitRunnerTask.RESULTS_FILE);
        if (StringUtils.isNotBlank(resultsFile))
        {
            if (!resultsFile.endsWith(".xml"))
            {
                errorCollection.addError(XunitRunnerTask.RESULTS_FILE, i18nResolver.getText("xunit.resultsFile.incorrectExtension"));
            }
        }
        else
        {
            errorCollection.addError(XunitRunnerTask.RESULTS_FILE, i18nResolver.getText("xunit.resultsFile.missing"));
        }
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        // Fill in defaults
        super.populateContextForCreate(context);
        context.put(XunitRunnerTask.RESULTS_FILE, DEFAULT_TEST_FILE);
    }

    @Override
    public void populateContextForView(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition)
    {
        super.populateContextForView(context, taskDefinition);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params, @Nullable TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> map = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(map, params, FIELDS_TO_COPY);
        return map;
    }

    public boolean taskProducesTestResults(@NotNull final TaskDefinition taskDefinition)
    {
        return true;
    }

    @NotNull
    @Override
    public Set<Requirement> calculateRequirements(@NotNull final TaskDefinition taskDefinition, @NotNull final Job job)
    {
        Set<Requirement> requirements = Sets.newHashSet();
        taskConfiguratorHelper.addSystemRequirementFromConfiguration(requirements, taskDefinition,
                XunitRunnerTask.LABEL, XunitRunnerTask.NUNIT_CAPABILITY_PREFIX);
        return requirements;
    }
}
